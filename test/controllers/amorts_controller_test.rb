require 'test_helper'

class AmortsControllerTest < ActionController::TestCase
  setup do
    @amort = amorts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:amorts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create amort" do
    assert_difference('Amort.count') do
      post :create, amort: {  }
    end

    assert_redirected_to amort_path(assigns(:amort))
  end

  test "should show amort" do
    get :show, id: @amort
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @amort
    assert_response :success
  end

  test "should update amort" do
    patch :update, id: @amort, amort: {  }
    assert_redirected_to amort_path(assigns(:amort))
  end

  test "should destroy amort" do
    assert_difference('Amort.count', -1) do
      delete :destroy, id: @amort
    end

    assert_redirected_to amorts_path
  end
end
