json.array!(@salespeople) do |salesperson|
  json.extract! salesperson, :id, :first_name, :last_name, :address, :phone
  json.url salesperson_url(salesperson, format: :json)
end
