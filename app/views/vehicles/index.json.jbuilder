json.array!(@vehicles) do |vehicle|
  json.extract! vehicle, :id, :vin, :year, :color_id, :make_id, :model_id, :type_id, :msrp, :cost
  json.url vehicle_url(vehicle, format: :json)
end
