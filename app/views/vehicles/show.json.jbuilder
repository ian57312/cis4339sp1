json.extract! @vehicle, :id, :vin, :year, :color_id, :make_id, :model_id, :type_id, :msrp, :cost, :created_at, :updated_at
