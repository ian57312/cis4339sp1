json.array!(@quotes) do |quote|
  json.extract! quote, :id, :vehicle_id, :customer_id, :salesperson_id, :surcharge, :discount, :price, :status
  json.url quote_url(quote, format: :json)
end
