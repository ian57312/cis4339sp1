module AmortsHelper
  def htmlrender(principal,int_rate,months)
    p = principal
    r = ((int_rate/100.to_f)/12).to_f
    n = months
    bal = principal
    pmt_amt = p * ((r*((1+r)**n))/(((1+r)**n)-1))
    pmt_int = r * bal
    pmt_p = pmt_amt - pmt_int
    amort_schedule = [number_to_currency(bal)]

    i = 0
    while i < n do
      pmt_int = r * bal
      bal = bal + pmt_int - pmt_amt
      pmt_p = pmt_amt - pmt_int
      amort_schedule.push(number_to_currency(pmt_amt),number_to_currency(pmt_int),number_to_currency(pmt_p),number_to_currency(bal))

      i += 1
    end

    i = 1
    j = 1
    htmlstring = "<table><thead><tr><th>Payment</th><th>Payment<br>Amount</th><th>Interest</th><th>Principal</th><th>Balance</th></tr></thead><tbody><tr><td><b>0</b></td><td></td><td></td><td></td><td>" + amort_schedule[0].to_s + "</td></tr>"
    bodystring = ""
    while i < amort_schedule.length do
      bodystring = bodystring + "<tr><td><b>" + j.to_s + "</b></td><td>" + amort_schedule[i].to_s + "</td><td>" + amort_schedule[i+1].to_s + "</td><td>" + amort_schedule[i+2].to_s + "</td><td>" + amort_schedule[i+3].to_s + "</td></tr>"
      i += 4
      j += 1
    end
    htmlstring = htmlstring + bodystring + "</tbody></table>"

    return htmlstring
  end
end
