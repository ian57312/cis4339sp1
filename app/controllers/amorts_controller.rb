class AmortsController < ApplicationController
  before_action :set_amort, only: [:show, :edit, :update, :destroy]

  # GET /amorts
  # GET /amorts.json
  def index
    @amorts = Amort.all
  end

  # GET /amorts/1
  # GET /amorts/1.json
  def show
  end

  # GET /amorts/new
  def new
    @amort = Amort.new
  end

  # GET /amorts/1/edit
  def edit
  end

  # POST /amorts
  # POST /amorts.json
  def create
    @amort = Amort.new(amort_params)

    respond_to do |format|
      if @amort.save
        format.html { redirect_to @amort, notice: 'Amort was successfully created.' }
        format.json { render :show, status: :created, location: @amort }
      else
        format.html { render :new }
        format.json { render json: @amort.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /amorts/1
  # PATCH/PUT /amorts/1.json
  def update
    respond_to do |format|
      if @amort.update(amort_params)
        format.html { redirect_to @amort, notice: 'Amort was successfully updated.' }
        format.json { render :show, status: :ok, location: @amort }
      else
        format.html { render :edit }
        format.json { render json: @amort.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /amorts/1
  # DELETE /amorts/1.json
  def destroy
    @amort.destroy
    respond_to do |format|
      format.html { redirect_to amorts_url, notice: 'Amort was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_amort
      @amort = Amort.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def amort_params
      params.require(:amort).permit(:principal, :int_rate, :months)
    end
end
