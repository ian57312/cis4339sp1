class Quote < ActiveRecord::Base
  has_many :customers
  has_many :vehicles
  has_many :salespeople

end
