class Customer < ActiveRecord::Base
  has_one :status
  belongs_to :quote

  def self.search(search)
    if search
      where(['last_name LIKE ? OR first_name LIKE ?', "%#{search}%","%#{search}%"])
    else
      all
    end
  end

  def fullname
    name = first_name + ' ' + last_name
  end
end
