class Vehicle < ActiveRecord::Base
  has_many :colors
  has_many :types
  has_many :makes
  has_many :models

  def self.search(search)
    if search
      where(['vin LIKE ?', "%#{search}%"])
    else
      all
    end
  end
end
