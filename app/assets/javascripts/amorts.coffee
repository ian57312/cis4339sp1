# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

build_table = ->
  principal =  $("#amort_principal").val()
  int_rate = $("#amort_int_rate").val()
  months = $("#amort_months").val()
  $.get "/amorts/amortarray",{principal: principal,int_rate: int_rate, months: months}

$ ->
  $('#amort_principal,#amort_int_rate,#amort_months').bind 'keyup mouseup mousewheel', ->
    build_table()

  build_table()