# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

updateNotes = ->
  selection_id = $('#quotes_vin').val()
  $.getJSON '/quotes/' + selection_id + '/notes', {},(json, response) ->
    $('#notes').text json['notes']

$ ->

  if $('#quotes_vin').length > 0
    updateNotes()
  $('#quotes_vin').change -> updateNotes()