class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.string :vin
      t.integer :year
      t.integer :color_id
      t.integer :make_id
      t.integer :model_id
      t.integer :type_id
      t.float :msrp
      t.float :cost

      t.timestamps null: false
    end
  end
end
