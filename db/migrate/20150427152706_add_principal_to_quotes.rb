class AddPrincipalToQuotes < ActiveRecord::Migration
  def change
    add_column :quotes, :principal, :float
  end
end
