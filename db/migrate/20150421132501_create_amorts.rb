class CreateAmorts < ActiveRecord::Migration
  def change
    create_table :amorts do |t|
      t.float :principal
      t.decimal :int_rate
      t.integer :months

      t.timestamps null: false
    end
  end
end
