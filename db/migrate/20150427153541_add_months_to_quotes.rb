class AddMonthsToQuotes < ActiveRecord::Migration
  def change
    add_column :quotes, :months, :integer
  end
end
