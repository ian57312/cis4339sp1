class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :vehicle_id
      t.integer :customer_id
      t.integer :salesperson_id
      t.float :price
      t.boolean :status

      t.timestamps null: false
    end
  end
end
