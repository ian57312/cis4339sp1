class CreateSalespeople < ActiveRecord::Migration
  def change
    create_table :salespeople do |t|
      t.string :first_name
      t.string :last_name
      t.string :address
      t.string :phone

      t.timestamps null: false
    end
  end
end
