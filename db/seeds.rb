# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

colors = Color.create([{name: 'Black'}, {name: 'Blue'}, {name: 'Green'}, {name: 'Red'}, {name: 'Silver'}, {name: 'White'}, {name: 'Yellow'}])
customers = Customer.create([{first_name: 'Ian', last_name: 'Smith', address: '3323 McCue Rd Apt 221, Houston, TX 77056', phone: '832-741-2785', :status_id => 1},
                             {first_name: 'Kent', last_name: 'Smith', address: '707 Noble Court, West Lafayette, IN 47906', phone: '765-423-9954', :status_id => 2},
                             {first_name: 'Micah', last_name: 'Maassen', address: '3323 McCue Rd Apt 221, Houston, TX 77056', phone: '713-621-4315', :status_id => 1},
                             {first_name: 'Ben', last_name: 'Grovak', address: '1100 Liberty Lane, Lafayette, IN 47905', phone: '765-093-4447', :status_id => 2},
                             {first_name: 'Mehran', last_name: 'Latif', address: '500 Cherry Blossom Drive, Munster, IN 46321', phone: '765-540-1015', :status_id => 1}])
salespeople = Salesperson.create([{first_name: 'Kevin', last_name: 'Owens', address: '5055 Forsyth Commerce Rd, Orlando, FL 32807', phone: '717-522-4384'},
                             {first_name: 'Sasha', last_name: 'Banks', address: '5055 Forsyth Commerce Rd, Orlando, FL 32807', phone: '717-522-4384'},
                             {first_name: 'Becky', last_name: 'Lynch', address: '5055 Forsyth Commerce Rd, Orlando, FL 32807', phone: '717-522-4384'},
                             {first_name: 'Tyler', last_name: 'Breeze', address: '5055 Forsyth Commerce Rd, Orlando, FL 32807', phone: '717-522-4384'},
                             {first_name: 'Hideo', last_name: 'Itami', address: '5055 Forsyth Commerce Rd, Orlando, FL 32807', phone: '717-522-4384'}])
makes = Make.create([{name: 'Acura'}, {name: 'Audi'}, {name: 'BMW'}, {name: 'Buick'},
                     {name: 'Cadillac'}, {name: 'Chevrolet'}, {name: 'Chrysler'}, {name: 'Dodge'},
                     {name: 'Ferrari'}, {name: 'Ford'}, {name: 'Honda'}, {name: 'Hyundai'},
                     {name: 'Infiniti'}, {name: 'Isuzu'}, {name: 'Jaguar'}, {name: 'Jeep'},
                     {name: 'Kia'}, {name: 'Lexus'}, {name: 'Lincoln'}, {name: 'Mazda'},
                     {name: 'Mercedes-Benz'}, {name: 'Mitsubishi'}, {name: 'Nissan'}, {name: 'Pontiac'},
                     {name: 'Saturn'}, {name: 'Scion'}, {name: 'Subaru'}, {name: 'Suzuki'},
                     {name: 'Toyota'}, {name: 'Volkswagen'}, {name: 'Volvo'}])
models = Model.create([{make_id: Make.find_by_name('Acura').id, name: 'NSX'},
                       {make_id: Make.find_by_name('Dodge').id, name: 'Dart'},
                       {make_id: Make.find_by_name('Dodge').id, name: 'Stratus'},
                       {make_id: Make.find_by_name('Jeep').id, name: 'Liberty'},
                       {make_id: Make.find_by_name('Mitsubishi').id, name: 'Lancer'},
                       {make_id: Make.find_by_name('Mitsubishi').id, name: 'Spider'},
                       {make_id: Make.find_by_name('Toyota').id, name: 'Camry'},
                       {make_id: Make.find_by_name('Toyota').id, name: 'Corolla'},
                       {make_id: Make.find_by_name('Volkswagen').id, name: 'Jetta'}])
statuses = Status.create([{name: 'Current'},{name: 'Prospect'}])
types = Type.create([{name: 'Car'}, {name: 'Electric'}, {name: 'Hybrid'}, {name: 'Recreational Vehicle (RV)'}, {name: 'Truck'}])
#vehicles = Vehicle.create([{vin: '1GTHG396X91505025'},{:year => 1999},{color_id: Color.find_by_name('Red').id},{make_id: Make.find_by_name('Acura').id},{model_id: Model.find_by_name('NSX').id},
#                          {type_id: Type.find_by_name('Car').id},{:msrp => 2500.00},{:cost => 500.00}])